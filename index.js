// Import des modules
const express = require('express');
const cors = require('cors');
const bcrypt = require('bcrypt');
const connection = require('./config/db')

// Creation de l'objet app qui gère les requetes et les reponses entre le clien et le serveur
const app = express();

// Configuration de l'app pour parser les requêtes au format JSON
app.use(express.json());

// Configuration de l'app pour parser les requêtes au format URL
app.use(express.urlencoded({extended: true,}));

// Définition du port du serveur back-end
const port = 3000;

app.listen(port, () => {
    console.log(`https://localhost:${port}`)
})