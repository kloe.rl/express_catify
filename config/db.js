// Import du module mysql2
const mysql = require('mysql2');

// Import de dotenv
require('dotenv').config();

// Configuration de la connexion à la BDD
const connection = mysql.createPool({    
    host:process.env.DB_HOST,
    user:process.env.DB_USER,
    database:process.env.DB_DATABASE,
    password:process.env.DB_PASSWORD
});

// Etablissement de la connexion 
connection.getConnection((err) => {
    if (err instanceof Error) {
        console.log('getConnection error:', err)
        return;
    }
})
